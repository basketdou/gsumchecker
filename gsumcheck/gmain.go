package main

import (
    "fmt"
    "strings"
    "fyne.io/fyne/v2"
    "fyne.io/fyne/v2/app"
    "fyne.io/fyne/v2/container"
    "fyne.io/fyne/v2/dialog"
    "fyne.io/fyne/v2/widget"
)
var sumtype string
func main() {
    a := app.New()
    w := a.NewWindow("gsumcheck")

    entryone := widget.NewEntry()
    entryone.SetPlaceHolder("The original checksum")
    entrytwo := widget.NewEntry()
    entrytwo.SetPlaceHolder("The checksum of modified file")
    entrythree := widget.NewRadioGroup([]string{"sha1", "md5"}, func(value string){
        sumtype = value
    })
    wcontent := container.NewVBox(entryone, entrytwo, entrythree, widget.NewButton("Compare", func() {
        if sumtype == "sha1" {
            if sha1check(entryone.Text, entrytwo.Text) == true {
            dialog.ShowInformation("Result of the check", "These sha1 sums are equal", w)
            } else {
                dialog.ShowInformation("Result of the check", "These sha1 sums are not equal", w)
            }
        }
        if sumtype == "md5" {
            if md5check(entryone.Text, entrytwo.Text) == true {
                dialog.ShowInformation("Result of the check", "These md5 sums are equal", w)
        } else {
            dialog.ShowInformation("Result of the check", "These md5 sums are not equal", w)
        }
        }
    }))
    w.SetContent(wcontent)
    w.Resize(fyne.NewSize(250, 250))
    w.Show()
    a.Run()
    tidyUp()
}

func tidyUp() {
    fmt.Println("Exited")
}

func sha1check(filesum string, exsum string) bool {
    y := strings.Compare(filesum, exsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}

func md5check(filesum string, exsum string) bool {
    y := strings.Compare(filesum, exsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}
