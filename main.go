package main
import (
    "bytes"
    "crypto/sha1"
    "crypto/md5"
    "encoding/hex"
    "flag"
    "fmt"
    "io"
    "os"
)
func l10n(message string, lang string) {
    if lang == "en" {
        if message == "err" {
            fmt.Println("A error occured.")
            fmt.Println("The error message: ")
        }
        if message == "origchecksumerr" {
            fmt.Println("Error: the original file checksum is missing.")
        }
        if message == "inputerr" {
            fmt.Println("Sorry, I don't understand you")
        }
        if message == "mresult" {
            fmt.Println("The result of this comparison: ")
        }
        if message == "equalcsums" {
            fmt.Println("The checksums are equal: ")
        }
        if message == "fchecksum" {
            fmt.Print("The sum of the current file: ")
        }
    }
    if lang == "de" {
        if message == "err" {
            fmt.Println("Ein Fehler ist passiert.")
            fmt.Println("Die Fehlermeldung: ")
        }
        if message == "origchecksumerr" {
            fmt.Println("Fehler: die Originalfileprüfsumme ist fehlend.")
        }
        if message == "inputerr" {
            fmt.Println("Entschuldigung, ich verstehe Sie nicht.")
        }
        if message == "mresult" {
            fmt.Println("Das Resultat dieses Vergleich: ")
        }
        if message == "equalcsums" {
            fmt.Println("Diese Prüfsummen sind gleich: ")
        }
        if message == "fchecksum" {
            fmt.Print("Die Prüfsumme des Files: ")
        }
    }
}
        
func handleerr(err error, lang string) {
    if err != nil {
        l10n("err", lang)
        fmt.Println(err)
    }
}
func sha1gen(filename string, lang string) []byte {
    inp, err := os.Open(filename)
    handleerr(err, lang)
    defer inp.Close()
    s := sha1.New()
    if _, err := io.Copy(s,inp); err != nil {
        handleerr(err, lang)
    }
    filesum := s.Sum(nil)
    l10n("fchecksum", lang)
    fmt.Printf("%x\n", filesum)
    return filesum
}
func sha1check(filesum []byte, exsum string, lang string) bool {
    desiredsum, err2 := hex.DecodeString(exsum)
    handleerr(err2, lang)
    y := bytes.Compare(filesum, desiredsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}
func md5gen(filename string, lang string) []byte {
    inp, err := os.Open(filename)
    handleerr(err, lang)
    defer inp.Close()
    m := md5.New()
    if _, err := io.Copy(m, inp); err != nil {
        handleerr(err, lang)
    }
    filesum := m.Sum(nil)
    l10n("fchecksum", lang)
    fmt.Printf("%x\n", filesum)
    return filesum
}
func md5check(filesum []byte, exsum string) bool {
    expectedsum, _ := hex.DecodeString(exsum)
    y := bytes.Compare(filesum, expectedsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}
func compareresult(result bool, lang string) {
    l10n("mresult", lang)
    l10n("equalcsums",lang)
    fmt.Println(result)
}
func main() {
    //vptr := flag.Bool("-verbose", false, "Verbose output")
    fileptr := flag.String("i", "foo", "File to check")
    //txtptr := flag.String("-text", "foo", "Text to check")
    origsumptr := flag.String("orig", "foo", "Original checksum of file")
    typeptr := flag.String("t", "foo", "A type of checksum\n Available types: md5, sha1")
    langptr := flag.String("l", "en", "A language that the tool will use to show messages in.")
    flag.Parse()
    if *fileptr != "foo" {
        if *origsumptr != "foo" {
            if *typeptr == "md5" {
                x := md5gen(*fileptr, *langptr)
                result := md5check(x, *origsumptr)
                compareresult(result, *langptr)
            }
            if *typeptr == "sha1" {
                x := sha1gen(*fileptr, *langptr)
                result := sha1check(x, *origsumptr, *langptr)
                compareresult(result, *langptr)
            } 
            if *typeptr == "foo" {
                l10n("inputerr", *langptr)
            }
        } 
        if *origsumptr == "foo" {
            l10n("origchecksumerr", *langptr)
        }
    }
}
