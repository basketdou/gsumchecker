package main

import (
    "os"
    "strings"
    //"crypto/sha1"
    //"crypto/md5"
    
    "github.com/therecipe/qt/widgets"
)
func main() {
        sumapp := widgets.NewQApplication(len(os.Args), os.Args)
        window := widgets.NewQMainWindow(nil, 0)
        window.SetMinimumSize2(450, 400)
        window.SetWindowTitle("qsumchecker")
        
        widget := widgets.NewQWidget(nil, 0)
        widget.SetLayout(widgets.NewQVBoxLayout())
        window.SetCentralWidget(widget)
        
        inpfield := widgets.NewQLineEdit(nil)
        inpfield.SetPlaceholderText("The original checksum")
        widget.Layout().AddWidget(inpfield)
        
        inpfield2 := widgets.NewQLineEdit(nil)
        inpfield2.SetPlaceholderText("The checksum of modified file")
        widget.Layout().AddWidget(inpfield2)
        
        inpsumtype := widgets.NewQLineEdit(nil)
        inpsumtype.SetPlaceholderText("A checksum type (supported: sha1, md5)")
        widget.Layout().AddWidget(inpsumtype)
        
        compbutton := widgets.NewQPushButton2("Compare", nil)
        compbutton.ConnectClicked(func(bool) {
            if inpsumtype.Text() == "sha1" {
                if sha1check(inpfield.Text(), inpfield2.Text()) == true {
                    widgets.QMessageBox_Information(nil, "The result", "The checksums are equal", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                } else {
                    widgets.QMessageBox_Information(nil, "The result", "The checksums are not equal", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
            if inpsumtype.Text() == "md5" {
                if md5check(inpfield.Text(), inpfield2.Text()) == true {
                    widgets.QMessageBox_Information(nil, "The result", "The checksums are equal", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                } else {
                    widgets.QMessageBox_Information(nil, "The result", "The checksums are not equal", widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
                }
            }
            //widgets.QMessageBox_Information(nil, "OK", inpfield.Text(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
            //widgets.QMessageBox_Information(nil, "OK", inpfield2.Text(), widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
        })
        widget.Layout().AddWidget(compbutton)
        
        window.Show()
        
        sumapp.Exec()
}

func sha1check(filesum string, exsum string) bool {
   
    
    y := strings.Compare(filesum, exsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}

func md5check(filesum string, exsum string) bool {
    y := strings.Compare(filesum, exsum)
    if y == 0 {
        return true
    } else {
        return false
    }
}
